import { test, expect } from "@jest/globals";

import { CreateAquariumManager } from "../CreateAquariumManager";
import { ErrorMsg } from "../enum/ErrorMsg";

test("Throw File Not Found", () => {
  expect(() =>
    CreateAquariumManager.createAquarium("tests/res/JON.json")
  ).toThrow(ErrorMsg.FILE_NOT_FOUND);
});

test("Throw Not a Json File", () => {
  expect(() =>
    CreateAquariumManager.createAquarium("tests/res/DUMMY.txt")
  ).toThrow(ErrorMsg.FILE_NOT_JSON);
});

test("Throw Not an Aquarium", () => {
  expect(() =>
    CreateAquariumManager.createAquarium("tests/res/DUMMY.json")
  ).toThrow(ErrorMsg.FILE_NOT_AQUARIUM);
});

test("Create Aquarium", () => {
  const aquarium = CreateAquariumManager.createAquarium("myAquarium.json");
  expect(aquarium).toBeDefined();
});
