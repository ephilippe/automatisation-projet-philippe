import { test, expect } from "@jest/globals";

import { CreateAquariumManager } from "../CreateAquariumManager";
import Aquarium from "../Aquarium";

test("Count enough shrimps", () => {
  const aquarium = CreateAquariumManager.createAquarium(
    "tests/res/AQUARIUM.json"
  );
  const myAquarium = new Aquarium(aquarium);
  expect(myAquarium.getShrimpsPopulation()).toStrictEqual(14);
});

test("Count enough fishes", () => {
  const aquarium = CreateAquariumManager.createAquarium(
    "tests/res/AQUARIUM.json"
  );
  const myAquarium = new Aquarium(aquarium);
  expect(myAquarium.getFishesPopulation()).toStrictEqual(5);
});

test("Count enough population", () => {
  const aquarium = CreateAquariumManager.createAquarium(
    "tests/res/AQUARIUM.json"
  );
  const myAquarium = new Aquarium(aquarium);
  expect(myAquarium.getAquariumPopulation()).toStrictEqual(19);
});
