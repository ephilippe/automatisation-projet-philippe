export interface Fish {
  species: string;
  length: number;
  color: string;
}
