export interface AquariumData {
  SHRIMPS: Shrimps[];
  FISHES: Fishes[];
}

interface Shrimps {
  name: string;
  quantity: number;
}

interface Fishes {
  name: string;
  quantity: number;
}
