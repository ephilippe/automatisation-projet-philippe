import { existsSync, readFileSync } from "fs";
import { AquariumData } from "./models/AquariumData";
import { ErrorMsg } from "./enum/ErrorMsg";

export class CreateAquariumManager {
  static createAquarium(filename: string): AquariumData {
    if (!existsSync(__dirname + "/" + filename))
      throw new Error(ErrorMsg.FILE_NOT_FOUND);

    if (!filename.endsWith(".json")) throw new Error(ErrorMsg.FILE_NOT_JSON);

    // Open the JSON file and parse it into an AquariumData object
    const data: AquariumData = JSON.parse(
      readFileSync(__dirname + "/" + filename, "utf-8")
    );

    if (!("AQUARIUM" in data)) throw new Error(ErrorMsg.FILE_NOT_AQUARIUM);

    const aquarium = data.AQUARIUM as AquariumData;

    if (!("SHRIMPS" in aquarium) || !("FISHES" in aquarium))
      throw new Error(ErrorMsg.FILE_NOT_AQUARIUM);

    return data.AQUARIUM as AquariumData;
  }
}
