import Aquarium from "./Aquarium";
import { CreateAquariumManager } from "./CreateAquariumManager";

const aquariumData = CreateAquariumManager.createAquarium("../myAquarium.json");
const myAquarium = new Aquarium(aquariumData);

console.log(
  `Vous avez ${myAquarium.getAquariumPopulation()} animaux dans votre aquarium`
);
