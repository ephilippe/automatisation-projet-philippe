import { AquariumData, Fishes, Shrimps } from "./models/AquariumData";

export default class Aquarium {
  private _aquarium: AquariumData;

  constructor(aquarium: AquariumData) {
    this._aquarium = aquarium;
    console.log(aquarium);
  }

  get aquarium(): AquariumData {
    return this._aquarium;
  }

  set aquarium(aquarium: AquariumData) {
    this._aquarium = aquarium;
  }

  public getShrimps = (): Shrimps[] => {
    return this._aquarium.SHRIMPS;
  };

  public getFishes = (): Fishes[] => {
    return this._aquarium.FISHES;
  };

  public getShrimpsPopulation = (): number => {
    let population = 0;
    for (const shrimps of this._aquarium.SHRIMPS) {
      population += shrimps.quantity;
    }
    return population;
  };

  public getFishesPopulation = (): number => {
    let population = 0;
    for (const fishes of this._aquarium.FISHES) {
      population += fishes.quantity;
    }
    return population;
  };

  public getAquariumPopulation = (): number => {
    return this.getShrimpsPopulation() + this.getFishesPopulation();
  };
}
