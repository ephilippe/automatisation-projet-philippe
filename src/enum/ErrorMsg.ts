export enum ErrorMsg {
  FILE_NOT_FOUND = "File does not exist",
  FILE_NOT_JSON = "File is not a JSON file",
  FILE_NOT_AQUARIUM = "File is not a valid aquarium",
}
