# Compte-Rendu CI/CD - Projet AQUARIUM

<img src="https://media.discordapp.net/attachments/889899584120315975/1174762029286952960/image.png?ex=6568c565&is=65565065&hm=7d15cb805cfb98ce65a03487afed7313eab14e317f8bc4762beebf7f443db89b&=&width=381&height=120">

## Introduction

Ce document décrit les mécanismes d'orchestration utilisés dans le pipeline CI/CD du projet AQUARIUM. Le pipeline est configuré à l'aide de GitLab CI pour automatiser le processus de développement, de test, et de déploiement.

## Sommaire

- [Compte-Rendu CI/CD - Projet AQUARIUM](#compte-rendu-cicd---projet-aquarium)
  - [Introduction](#introduction)
  - [Sommaire](#sommaire)
  - [Prérequis](#prérequis)
  - [Installation](#installation)
  - [Utilisation](#utilisation)
  - [Disclaimer](#disclaimer)
  - [Étapes du Pipeline](#étapes-du-pipeline)
    - [Préparation de l'environnement](#préparation-de-lenvironnement)
    - [Notification Discord](#notification-discord)
    - [Lint](#lint)
    - [Build](#build)
    - [Tests unitaires](#tests-unitaires)
    - [Test Docker](#test-docker)
    - [Génération de la documentation](#génération-de-la-documentation)
    - [Déploiement](#déploiement)
  - [Lien vers les Résultats de Pipeline](#lien-vers-les-résultats-de-pipeline)

## Disclaimer

La pipeline ne termine pas en raison d'un manque d'espace sur les différents runners. La pipeline ne peut donc pas être testée.

```bash
> npm WARN tar TAR_ENTRY_ERROR ENOSPC: no space left on device, write
```

## Étapes du Pipeline

### Préparation de l'environnement

Une image Node.js est utilisée pour la préparation de l'environnement. Les dépendances nécessaires sont installées, y compris les packages npm requis.

### Notification Discord

Une notification est envoyée sur un canal Discord en fonction du statut du job CI/CD. En cas de succès, un message positif est envoyé. En cas d'échec, un message d'erreur est envoyé.

### Lint

Cette étape est responsable de l'analyse statique du code source. Elle vérifie que le code source est conforme aux règles de codage définies dans le fichier .eslintrc.json.

### Build

Cette étape est responsable de la compilation du code source. Les dépendances sont installées, et le script de build génère un artefact dans le dossier "dist/".

### Tests unitaires

Les tests sont exécutés à l'aide de Jest pour s'assurer de la qualité du code. Cette étape dépend de la phase de build.

### Test Docker

Cette étape construit une image Docker du projet et la lance en tant que conteneur Docker. Elle est marquée avec des tags spécifiques et s'exécute sur des étiquettes prédéfinies.

### Génération de la documentation

La documentation est générée à l'aide de Typedoc. Cette étape s'effectue uniquement lorsque le code est poussé sur la branche principale.

### Déploiement

Une image Debian est utilisée pour le déploiement. Cette étape utilise SSH pour se connecter à un serveur distant et déploie le projet. La clé privée de déploiement est sécurisée dans les variables GitLab CI. Cette étape est pour l'instant camouflée par une simple commande whoami car le serveur et les runners sont pleins et la pipeline ne peut pas être testée.

## Lien vers les Résultats de Pipeline

# Lorsque les runners étaient encore disponibles

- [Pipeline réussie avec des Tests réussis](https://gitlab.iut-blagnac.fr/ephilippe/automatisation-projet-philippe/-/commit/8995040a8183b8983dfd76eea0cab0efc7ec1f06)
- [Pipeline échouée avec des Tests échoués](https://gitlab.iut-blagnac.fr/ephilippe/automatisation-projet-philippe/-/commit/89775feb30ad78a02591a6a29d7506e2589ed3e9)
